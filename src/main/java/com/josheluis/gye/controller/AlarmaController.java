package com.josheluis.gye.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.josheluis.gye.entity.Medicamento;
import com.josheluis.gye.entity.MedicamentoObjectId;
import com.josheluis.gye.entity.ResponseObject;
import com.josheluis.gye.entity.Usuario;
import com.josheluis.gye.entity.UsuarioLogin;
import com.josheluis.gye.service.MedicamentoService;
import com.josheluis.gye.service.NovedadesService;
import com.josheluis.gye.service.UsuarioService;

import lombok.extern.log4j.Log4j2;

@Log4j2
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/alarmaMedicamentos")
public class AlarmaController {
	@Autowired
	UsuarioService usuarioService;
	
	@Autowired
	MedicamentoService medicamentosService;
	
	@Autowired
	NovedadesService novedadesrepo;
	
	@PostMapping("/registrarUsuario")
	public ResponseObject registrarUsuario(@RequestBody @Valid  Usuario usuario ) {

		try {
			return usuarioService.insertUsuario(usuario);
		} catch (Exception e) {
			log.error("RegistrarUsuario: {}",e);
			ResponseObject responseObject = new ResponseObject();
			responseObject.setIdResult(-1L);
			responseObject.setMensajeDescripcion("Error al Insertar usuario");
			return responseObject;
		}
	}
	@PostMapping("/iniciarSession")
	public ResponseObject iniciarSession(@RequestBody @Valid  UsuarioLogin login ) 
	{
		try {
			return usuarioService.iniciarSession(login);
		} catch (Exception e) {
			log.error("RegistrarUsuario: {}",e);
			ResponseObject responseObject = new ResponseObject();
			responseObject.setIdResult(-1L);
			responseObject.setMensajeDescripcion("Error al Insertar usuario");
			return responseObject;
		}
	}
	@PostMapping("/registraMedicamento")
	public ResponseObject registraMedicamento(@RequestBody @Valid Medicamento medicamento) 
	{
		try {
			return medicamentosService.registraMedicamento(medicamento);
		} catch (Exception e) {
			log.error("RegistrarUsuario: {}",e);
			ResponseObject responseObject = new ResponseObject();
			responseObject.setIdResult(-1L);
			responseObject.setMensajeDescripcion("Error al Insertar medicamento");
			return responseObject;
		}
	}
	
	@PostMapping("/actualizarMedicamento")
	public ResponseObject actualizarMedicamento(@RequestBody @Valid Medicamento medicamento) 
	{
		try {
			return medicamentosService.actualizarMedicamento(medicamento);
		} catch (Exception e) {
			log.error("RegistrarUsuario: {}",e);
			ResponseObject responseObject = new ResponseObject();
			responseObject.setIdResult(-1L);
			responseObject.setMensajeDescripcion("Error al actualizar medicamento");
			return responseObject;
		}
	}
	
	@PostMapping("/consumeMedicamento")
	public ResponseObject consumeMedicamento(@RequestBody @Valid MedicamentoObjectId medicamentoId) 
	{
		try {
			return medicamentosService.consumeMedicamento(medicamentoId.getMedicamentoId());
		} catch (Exception e) {
			log.error("RegistrarUsuario: {}",e);
			ResponseObject responseObject = new ResponseObject();
			responseObject.setIdResult(-1L);
			responseObject.setMensajeDescripcion("Error al actualizar medicamento");
			return responseObject;
		}
	}
	
	@PostMapping("/findMedicamentos")
	public ResponseObject findMedicamentos(@RequestBody @Valid MedicamentoObjectId medicamentoId) 
	{
		try {
			return medicamentosService.findMedicamentos(medicamentoId.getUsuarioId());
		} catch (Exception e) {
			log.error("RegistrarUsuario: {}",e);
			ResponseObject responseObject = new ResponseObject();
			responseObject.setIdResult(-1L);
			responseObject.setMensajeDescripcion("Error al actualizar medicamento");
			return responseObject;
		}
	}
	@PostMapping("/findNovedades")
	public ResponseObject findNovedades(@RequestBody @Valid MedicamentoObjectId medicamentoId) 
	{
		try {
			return novedadesrepo.findNovedades(medicamentoId.getUsuarioId());
		} catch (Exception e) {
			log.error("RegistrarUsuario: {}",e);
			ResponseObject responseObject = new ResponseObject();
			responseObject.setIdResult(-1L);
			responseObject.setMensajeDescripcion("Error al actualizar medicamento");
			return responseObject;
		}
	}
}
