package com.josheluis.gye.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.josheluis.gye.entity.ResponseObject;
import com.josheluis.gye.entity.Usuario;
import com.josheluis.gye.entity.UsuarioLogin;
import com.josheluis.gye.repository.UsuarioRepository;

import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
public class UsuarioService {

	@Autowired
	UsuarioRepository usuarioRepository;
	
	public ResponseObject insertUsuario(Usuario usuario) {
		ResponseObject responseObject = new ResponseObject();
		try {
			usuario.setUsuarioId(Long.valueOf(usuario.getCodPaisId().toString()+usuario.getCelular().toString()));
			List<Usuario> usuarios = usuarioRepository.findByUsuarioId(usuario.getUsuarioId());
			if(usuarios.stream().count()>0) {
				responseObject.setIdResult(1L);
				responseObject.setMensajeDescripcion("Usuario ya registrado");
				responseObject.getUsuarios().addAll(usuarios);
			}else {
				responseObject.setIdResult(0L);
				responseObject.setMensajeDescripcion("Usuario Registrado con Exito");
				responseObject.getUsuarios().add(usuarioRepository.save(usuario));
			}
			return responseObject;
		} catch (Exception e) {
			log.error("insertUsuario",e.getMessage());
			responseObject.setIdResult(-1L);
			responseObject.setMensajeDescripcion("Error al Insertar usuario");
			return responseObject;
		}
	}
	
	public ResponseObject iniciarSession(UsuarioLogin login) {
		ResponseObject responseObject = new ResponseObject();
		try {
			login.setUsuarioId(Long.valueOf(login.getCodPaisId().toString()+login.getCelular().toString()));
			List<Usuario> usuarios = usuarioRepository.findByUsuarioIdAndContrasenaAndEstado(login.getUsuarioId(), login.getContrasena(),"A");
			if(usuarios.stream().count()>0) {
				responseObject.setIdResult(1L);
				responseObject.setMensajeDescripcion("Credenciales Correctas");
				responseObject.getUsuarios().addAll(usuarios);
			}else {
				responseObject.setIdResult(-1L);
				responseObject.setMensajeDescripcion("Credenciales incorrectas");
			}
			return responseObject;
		} catch (Exception e) {
			log.error("insertUsuario",e.getMessage());
			responseObject.setIdResult(-1L);
			responseObject.setMensajeDescripcion("Error al Iniciar Session");
			return responseObject;
		}
	}
}
