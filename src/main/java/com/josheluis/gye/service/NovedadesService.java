package com.josheluis.gye.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.josheluis.gye.entity.ResponseObject;
import com.josheluis.gye.repository.NovedadRepository;

import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
public class NovedadesService {

	@Autowired
	NovedadRepository novedadRepository;
	public ResponseObject findNovedades(Long UsuarioId) 
	{
		ResponseObject responseObject = new ResponseObject();
		try {
			responseObject.setIdResult(0L);
			responseObject.setMensajeDescripcion("Consulta correctamente ejecutada");
			responseObject.getNovedads().addAll(novedadRepository.findByUsuarioId(UsuarioId));
			return responseObject;
		} catch (Exception e) {
			log.error("insertUsuario",e.getMessage());
			responseObject.setIdResult(-1L);
			responseObject.setMensajeDescripcion("Error al buscar usuarios");
			return responseObject;
		}
		 
	}
}
