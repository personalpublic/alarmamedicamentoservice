package com.josheluis.gye.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.josheluis.gye.entity.Medicamento;
import com.josheluis.gye.entity.ResponseObject;
import com.josheluis.gye.repository.MedicamentoRepository;

import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
public class MedicamentoService {
	
	@Autowired
	MedicamentoRepository medicamentoRepository;
	
	public ResponseObject registraMedicamento(Medicamento medicamento) 
	{
		ResponseObject object = new ResponseObject();
		try {
			List<Medicamento> medicamentos =  medicamentoRepository.findByNombreMedicamentoAndGramosAndUsuarioId(medicamento.getNombreMedicamento(), medicamento.getGramos(),medicamento.getUsuarioId());
			if(medicamentos.stream().count()>0) 
			{
				object.setIdResult(1L);
				object.setMensajeDescripcion("Medicamento ya registrado");
				object.setMedicamentos(medicamentos);
			}else {
				medicamento.setMedicamentoId(generateID());
				object.setMensajeDescripcion("Medicamento registrado");
				object.setIdResult(0L);
				object.getMedicamentos().add(medicamentoRepository.save(medicamento));
			}
			return object;
		} catch (Exception e) {
			log.error("registraMedicamento {}",e);
			object.setMensajeDescripcion("Error al registrar Medicamento: "+e.getMessage());
			object.setIdResult(-1L);
			return object;
		}
	}
	public ResponseObject consumeMedicamento(Long medicamentoId) 
	{
		ResponseObject object = new ResponseObject();
		try {
			if(medicamentoId==null) 
			{
				object.setIdResult(-1L);
				object.setMensajeDescripcion("Identificador de Medicamento vacio");
				return object;
			}
			List<Medicamento> medicamentos =  medicamentoRepository.findByMedicamentoId(medicamentoId);
			medicamentos.stream().forEach(x->{
				x.setFechaActualizacion(new Date());
			});
			object.getMedicamentos().addAll(medicamentoRepository.saveAll(medicamentos));
			object.setIdResult(0L);
			object.setMensajeDescripcion("Medicamento actualizado");
			return object;
		} catch (Exception e) {
			log.error("consumeMedicamento {}",e);
			object.setMensajeDescripcion("Error al actualizar Medicamento: "+e.getMessage());
			object.setIdResult(-1L);
			return object;
		}
	}
	public ResponseObject actualizarMedicamento(Medicamento medicamento) 
	{
		ResponseObject object = new ResponseObject();
		try {
			if(medicamento.getMedicamentoId()==null) 
			{
				object.setIdResult(-1L);
				object.setMensajeDescripcion("Identificador de Medicamento vacio");
				return object;
			}
			List<Medicamento> medicamentos =  medicamentoRepository.findByMedicamentoId(medicamento.getMedicamentoId());
			if(medicamentos.stream().count()>0) 
			{
				object.setIdResult(0L);
				object.setMensajeDescripcion("Medicamento actualizado");
				medicamento.setFechaActualizacion(new Date());
				object.getMedicamentos().add(medicamentoRepository.save(medicamento));
				return object;
			}else {
				object.setIdResult(1L);
				object.setMensajeDescripcion("Medicamento no registrado");
				return object;
			}
			
		} catch (Exception e) {
			log.error("registraMedicamento {}",e);
			object.setMensajeDescripcion("Error al registrar Medicamento: "+e.getMessage());
			object.setIdResult(-1L);
			return object;
		}
	}
	private Long generateID() {
		return System.currentTimeMillis();
	}
	
	public ResponseObject findMedicamentos(Long usuarioId) 
	{
		ResponseObject object = new ResponseObject();
		
		try {
			object.setIdResult(0L);
			object.setMensajeDescripcion("Consulta correcta");
			List<Medicamento> medicamentos =  medicamentoRepository.findByUsuarioId(usuarioId);
			object.getMedicamentos().addAll(medicamentos);
			return object;
		} catch (Exception e) {
			log.error("registraMedicamento {}",e);
			object.setMensajeDescripcion("Error al buscar medicamentos: "+e.getMessage());
			object.setIdResult(-1L);
			return object;
		}
	}
	
}
