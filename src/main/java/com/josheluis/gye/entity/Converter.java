// To use this code, add the following Maven dependency to your project:
//
//     org.projectlombok : lombok : 1.18.2
//     com.fasterxml.jackson.core     : jackson-databind          : 2.9.0
//     com.fasterxml.jackson.datatype : jackson-datatype-jsr310   : 2.9.0
//
// Import this package:
//
//     import com.josheluis.gye.entity.Converter;
//
// Then you can deserialize a JSON string with
//
//     Novedad data = Converter.NovedadFromJsonString(jsonString);
//     Medicamento data = Converter.MedicamentoFromJsonString(jsonString);
//     Usuario data = Converter.UsuarioFromJsonString(jsonString);

package com.josheluis.gye.entity;

import java.io.IOException;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import java.time.OffsetDateTime;
import java.time.OffsetTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;

public class Converter {
    // Date-time helpers

    private static final DateTimeFormatter DATE_TIME_FORMATTER = new DateTimeFormatterBuilder()
            .appendOptional(DateTimeFormatter.ISO_DATE_TIME)
            .appendOptional(DateTimeFormatter.ISO_OFFSET_DATE_TIME)
            .appendOptional(DateTimeFormatter.ISO_INSTANT)
            .appendOptional(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SX"))
            .appendOptional(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ssX"))
            .appendOptional(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))
            .toFormatter()
            .withZone(ZoneOffset.UTC);

    public static OffsetDateTime parseDateTimeString(String str) {
        return ZonedDateTime.from(Converter.DATE_TIME_FORMATTER.parse(str)).toOffsetDateTime();
    }

    private static final DateTimeFormatter TIME_FORMATTER = new DateTimeFormatterBuilder()
            .appendOptional(DateTimeFormatter.ISO_TIME)
            .appendOptional(DateTimeFormatter.ISO_OFFSET_TIME)
            .parseDefaulting(ChronoField.YEAR, 2020)
            .parseDefaulting(ChronoField.MONTH_OF_YEAR, 1)
            .parseDefaulting(ChronoField.DAY_OF_MONTH, 1)
            .toFormatter()
            .withZone(ZoneOffset.UTC);

    public static OffsetTime parseTimeString(String str) {
        return ZonedDateTime.from(Converter.TIME_FORMATTER.parse(str)).toOffsetDateTime().toOffsetTime();
    }
    // Serialize/deserialize helpers

    public static Novedad NovedadFromJsonString(String json) throws IOException {
        return getNovedadObjectReader().readValue(json);
    }

    public static String NovedadToJsonString(Novedad obj) throws JsonProcessingException {
        return getNovedadObjectWriter().writeValueAsString(obj);
    }

    public static Medicamento MedicamentoFromJsonString(String json) throws IOException {
        return getMedicamentoObjectReader().readValue(json);
    }

    public static String MedicamentoToJsonString(Medicamento obj) throws JsonProcessingException {
        return getMedicamentoObjectWriter().writeValueAsString(obj);
    }

    public static Usuario UsuarioFromJsonString(String json) throws IOException {
        return getUsuarioObjectReader().readValue(json);
    }

    public static String UsuarioToJsonString(Usuario obj) throws JsonProcessingException {
        return getUsuarioObjectWriter().writeValueAsString(obj);
    }

    private static ObjectReader NovedadReader;
    private static ObjectWriter NovedadWriter;

    private static void instantiateNovedadMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.findAndRegisterModules();
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        SimpleModule module = new SimpleModule();
        module.addDeserializer(OffsetDateTime.class, new JsonDeserializer<OffsetDateTime>() {
            @Override
            public OffsetDateTime deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
                String value = jsonParser.getText();
                return Converter.parseDateTimeString(value);
            }
        });
        mapper.registerModule(module);
        NovedadReader = mapper.readerFor(Novedad.class);
        NovedadWriter = mapper.writerFor(Novedad.class);
    }

    private static ObjectReader getNovedadObjectReader() {
        if (NovedadReader == null) instantiateNovedadMapper();
        return NovedadReader;
    }

    private static ObjectWriter getNovedadObjectWriter() {
        if (NovedadWriter == null) instantiateNovedadMapper();
        return NovedadWriter;
    }

    private static ObjectReader MedicamentoReader;
    private static ObjectWriter MedicamentoWriter;

    private static void instantiateMedicamentoMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.findAndRegisterModules();
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        SimpleModule module = new SimpleModule();
        module.addDeserializer(OffsetDateTime.class, new JsonDeserializer<OffsetDateTime>() {
            @Override
            public OffsetDateTime deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
                String value = jsonParser.getText();
                return Converter.parseDateTimeString(value);
            }
        });
        mapper.registerModule(module);
        MedicamentoReader = mapper.readerFor(Medicamento.class);
        MedicamentoWriter = mapper.writerFor(Medicamento.class);
    }

    private static ObjectReader getMedicamentoObjectReader() {
        if (MedicamentoReader == null) instantiateMedicamentoMapper();
        return MedicamentoReader;
    }

    private static ObjectWriter getMedicamentoObjectWriter() {
        if (MedicamentoWriter == null) instantiateMedicamentoMapper();
        return MedicamentoWriter;
    }

    private static ObjectReader UsuarioReader;
    private static ObjectWriter UsuarioWriter;

    private static void instantiateUsuarioMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.findAndRegisterModules();
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        SimpleModule module = new SimpleModule();
        module.addDeserializer(OffsetDateTime.class, new JsonDeserializer<OffsetDateTime>() {
            @Override
            public OffsetDateTime deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
                String value = jsonParser.getText();
                return Converter.parseDateTimeString(value);
            }
        });
        mapper.registerModule(module);
        UsuarioReader = mapper.readerFor(Usuario.class);
        UsuarioWriter = mapper.writerFor(Usuario.class);
    }

    private static ObjectReader getUsuarioObjectReader() {
        if (UsuarioReader == null) instantiateUsuarioMapper();
        return UsuarioReader;
    }

    private static ObjectWriter getUsuarioObjectWriter() {
        if (UsuarioWriter == null) instantiateUsuarioMapper();
        return UsuarioWriter;
    }
}
