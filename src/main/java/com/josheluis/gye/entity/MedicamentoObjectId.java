package com.josheluis.gye.entity;

import lombok.Data;

@Data
public class MedicamentoObjectId {
	private Long medicamentoId;
	private Long usuarioId;
}
