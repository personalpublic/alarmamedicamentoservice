package com.josheluis.gye.entity;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;

public class UsuarioLogin {
	private Long usuarioId;
    @NotNull
    @PositiveOrZero
    private Long codPaisId;
    @NotNull
    @PositiveOrZero
    private Long celular;
    @NotNull
    private String contrasena;
	public Long getUsuarioId() {
		return usuarioId;
	}
	public void setUsuarioId(Long usuarioId) {
		this.usuarioId = usuarioId;
	}
	public Long getCodPaisId() {
		return codPaisId;
	}
	public void setCodPaisId(Long codPaisId) {
		this.codPaisId = codPaisId;
	}
	public Long getCelular() {
		return celular;
	}
	public void setCelular(Long celular) {
		this.celular = celular;
	}
	public String getContrasena() {
		return contrasena;
	}
	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}
}
