package com.josheluis.gye.entity;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@lombok.Data
@Document(collection = "Novedad")
public class Novedad {
	@Id
	private String _id;
    private String tipoNovedad;
    private Long usuarioId;
    private String nombre;
    private Long codPaisId;
    private Long celular;
    private String correo;
    private String contrasea;
    private Date fechaNacimiento;
    private Date fechaRegistro;
    private Date fechaActualizacion;
    private String estadoUsuario;
    private Long medicamentoId;
    private String nombreMedicamento;
    private Date fechaCaducidad;
    private Double gramos;
    private String descripcion;
    private Date horaUltimoConsumo;
    private Long frecuencia;
    private Date fechaInicioTratamiento;
    private Date fechaFinTratamiento;
    private String accionMedicamento;
    private String estadoMedicamento;
}
