package com.josheluis.gye.entity;

import java.util.Date;

import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@lombok.Data
@Document(collection = "Medicamento")
public class Medicamento {
	@Id
	private String _id;
    private Long medicamentoId;
    @NotNull
    private Long usuarioId;
    @NotNull
    private String nombreMedicamento;
    @NotNull
    @Future
    private Date fechaCaducidad;
    @NotNull
    private Double gramos;
    @NotNull
    private String descripcion;
    private Date horaUltimoConsumo;
    @NotNull
    private Long frecuencia;
    @NotNull
    private Date fechaInicioTratamiento;
    @Future
    private Date fechaFinTratamiento;
    @NotNull
    private Date fechaRegistro;
    private Date fechaActualizacion;
    @NotNull
    private String estado;
}
