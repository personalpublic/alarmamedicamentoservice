package com.josheluis.gye.entity;
import java.util.Date;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.PositiveOrZero;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@lombok.Data
@Document(collection = "Usuario")
public class Usuario {
	@Id
	private String _id;
    private Long usuarioId;
    @NotNull
    private String nombre;
    @NotNull
    @PositiveOrZero
    private Long codPaisId;
    @NotNull
    @PositiveOrZero
    private Long celular;
    @Email
    private String correo;
    @NotNull
    private String contrasena;
    @Past
    private Date fechaNacimiento;
    private Date fechaRegistro;
    private Date fechaActualizacion;
    @NotNull
    private String estado;
}
