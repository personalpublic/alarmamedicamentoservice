package com.josheluis.gye.entity;

import java.util.ArrayList;
import java.util.List;

@lombok.Data
public class ResponseObject {
    private Long idResult;
    private String mensajeDescripcion;
    private List<Medicamento> medicamentos = new ArrayList<Medicamento>();
    private List<Usuario> usuarios = new ArrayList<Usuario>();
    private List<Novedad> novedads = new ArrayList<Novedad>();
}
