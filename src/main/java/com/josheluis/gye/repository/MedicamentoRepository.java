package com.josheluis.gye.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.josheluis.gye.entity.Medicamento;

public interface MedicamentoRepository extends MongoRepository<Medicamento, String> {
	public List<Medicamento> findByNombreMedicamentoAndGramosAndUsuarioId(String nombreMedicamento,Double Gramos,Long usuario);
	public List<Medicamento> findByMedicamentoId(Long medicamentoId);
	public List<Medicamento> findByUsuarioId(Long usuarioId);
}
