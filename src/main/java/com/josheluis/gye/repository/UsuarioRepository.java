package com.josheluis.gye.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.josheluis.gye.entity.Usuario;

public interface UsuarioRepository extends MongoRepository<Usuario, String> {
	public List<Usuario> findByUsuarioId(Long usuarioId);
	public List<Usuario> findByUsuarioIdAndContrasenaAndEstado(Long usuarioId,String contrasena,String estado);
	
}
