package com.josheluis.gye.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.josheluis.gye.entity.Novedad;

public interface NovedadRepository extends MongoRepository<Novedad, String> {
	public List<Novedad> findByUsuarioId(Long usuarioId);
	
}
