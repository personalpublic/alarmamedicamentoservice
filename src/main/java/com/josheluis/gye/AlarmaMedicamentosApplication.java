package com.josheluis.gye;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AlarmaMedicamentosApplication {

	public static void main(String[] args) {
		SpringApplication.run(AlarmaMedicamentosApplication.class, args);
	}

}
